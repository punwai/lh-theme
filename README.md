This repository contains a theme made for [librehealth.io](https://gitlab.com/librehealth/librehealth.io). A demo can be found [here](https://punwai.gitlab.io/librehealththeme/).
LibreHealth is an open source project which develops Healthcare-based technologies. To contribute go to the [Github](https://github.com/LibreHealthIO) or [Gitlab](https://gitlab.com/librehealth)

### How to use
The project structure of the config file is similar to that of the universal theme. 
It contains the features similar to: Client, Carousel, Features, Nav, Footer and See More.

### Features

#### Config Params
The following fields can be set in the config's params
```
[params]
    enforceSSL = false
    viewMorePostLink     = "#"
    author = "LibreHealth"
    about_us = "LibreHealth is collaborative community for free & open source software projects in Health IT, and is a member project of Software Freedom Conservancy."
    copyright = "Copyright &copy; {year} Software Freedom Conservancy, Inc. All rights reserved."
    date_format = "2018-10-02"
    logo = "img/logo.png"
    sitedesc = "LibreHealth is an open source healthcare project"
    siteslogan = "Healthcare for Humanity"

```

{year} in copyright will be replaced by the current year.
siteslogan and sitedesc will be displayed in the Landing Page.

#### Carousel

The carousel content is configured in the data directory.

```
data
└── carousel
    ├── customizable.yaml
    ├── design.yaml
    ├── features.yaml
    └── multipurpose.yaml
```

Each carousel entry is represented as a YAML file inside `data/carousel`. Let's see the `reports.yaml` as an example of a carousel entry.

```yaml
weight: 4
title: "Easy to customize"
description: >
  <ul class="list-style-none">
    <li>7 preprepared colour variations.</li>
    <li>Easily to change fonts</li>
  </ul>
image: "img/carousel/template-easy-code.png"
```

The `weight` field determines the position of the entry. `title` is a text-only field. The `description` field accepts HTML code. And the `image` must contain the relative path to the image inside the `static` directory.

Once the carousel is configured, it must be explicitly enabled in the `config.toml` file.

```toml
[params.carousel]
    enable = true
```

#### Features

Features are also defined in the `data` directory just like the carousel.

```
data
└── features
    ├── consulting.yaml
    ├── email.yaml
    ├── print.yaml
    ├── seo.yaml
    ├── uiux.yaml
    └── webdesign.yaml
```

A feature file looks like this.

```yaml
weight: 4
name: "Consulting"
icon: "fa fa-lightbulb-o"
description: "Fifth abundantly made Give sixth hath. Cattle creature i be don't them behold green moved fowl Moved life us beast good yielding. Have bring."
```

The `icon` field is the CSS class of an icon. In this example we have used icons powered by [FontAwesome](http://fontawesome.io/icons/).

Once you have completed your features, enable them in the `config.toml` file.

```toml
[params.features]
    enable = true
```

#### Testimonials

Testimonials are defined in the `data` directory.

```
data
└── testimonials
    ├── 1.yaml
    ├── 2.yaml
    ├── 3.yaml
    ├── 4.yaml
    └── 5.yaml
```

You can add as many testimonials files as you want. Be sure you fill in all fields as in the following example.

```yaml
text: "One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections."
name: "John McIntyre"
position: "CEO, TransTech"
avatar: "img/testimonials/person-1.jpg"
```

Then, enable it in the configuration file and add a title and subtitle.

```toml
[params.testimonials]
    enable = true
    title = "Testimonials"
    subtitle = "We have worked with many clients and we always like to hear they come out from the cooperation happy and satisfied. Have a look what our clients said about us."
```


#### See more

This section is used to provide a link to another place. It can be an external site, or a page or post within your Hugo site.

You can enable it in the configuration file.

```toml
[params.see_more]
    enable = true
    icon = "fa fa-file-code-o"
    title = "Do you want to see more?"
    subtitle = "We have prepared for you more than 40 different HTML pages, including 5 variations of homepage."
    link_url = "http://your-site.com/more"
    link_text = "Check other homepages"
```


#### Clients

The clients section is used to show a list of logos of companies you have collaborated with. The clients are defined in the `data` directory as YAML files.

```
data
└── clients
    ├── 1.yaml
    ├── 2.yaml
    ├── 3.yaml
    ├── 4.yaml
    ├── 5.yaml
    └── 6.yaml
```

Each client file contains the following information.

```yaml
name: "customer-1"
image: "img/clients/customer-1.png"
url: "http://www.customer-1.com"
```

The `name` of the client. `image` is a relative path to the logo inside the `static` directory. And `url` is an optional field in case you want to link the logo to the client's website.

Then, you can enable the section in the configuration file.

```toml
[params.clients]
    enable = true
    title = "Our Partners"
    subtitle = "We have proudly collaborated with the following companies."
```

### Thanks
Universal-theme for providing a base for the theme.